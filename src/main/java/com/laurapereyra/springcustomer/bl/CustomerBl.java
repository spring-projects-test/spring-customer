package com.laurapereyra.springcustomer.bl;

import com.laurapereyra.springcustomer.dto.AccountDto;
import com.laurapereyra.springcustomer.dto.CustomerDto;
import com.laurapereyra.springcustomer.dto.RequestDto;
import com.laurapereyra.springcustomer.dto.ResponseDto;
import com.laurapereyra.springcustomer.mapper.CustomerMapper;
import com.laurapereyra.springcustomer.mapper.RequestMapper;
import com.laurapereyra.springcustomer.service.AccountServiceClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class CustomerBl {
    private CustomerMapper customerMapper;
    private RequestMapper requestMapper;
    private AccountServiceClient accountServiceClient;

    @Autowired
    public CustomerBl(CustomerMapper customerMapper, RequestMapper requestMapper, AccountServiceClient accountServiceClient) {
        this.customerMapper = customerMapper;
        this.requestMapper = requestMapper;
        this.accountServiceClient = accountServiceClient;
    }

    public CustomerDto getCustomerById(int customerId, RequestDto requestDto) {
        int requestPk = requestMapper.getPkRequest();
        requestDto.setRequestId(requestPk);
        requestMapper.persistRequest(requestDto);
        return customerMapper.getCustomerById(customerId);
    }

    public List<AccountDto> getAccount(int customerId){
        ResponseDto<?> result = accountServiceClient.customerClient(customerId);
        System.out.println("asd: " + result);
        if (!result.getStatusCode().endsWith("0000")) {
            System.out.println("Sin datos");
            List<AccountDto> error = new ArrayList<>();
            return error;
        }
        return (List<AccountDto>) result.getResponse();
    }

    public void customerRegister(CustomerDto customerDto){
        int pk = customerMapper.getCustomerPk();
        customerDto.setCustomerId(pk);
        customerMapper.registerCustomer(customerDto);
    }
}
