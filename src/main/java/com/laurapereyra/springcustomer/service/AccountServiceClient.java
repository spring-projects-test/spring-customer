package com.laurapereyra.springcustomer.service;

import com.laurapereyra.springcustomer.dto.AccountDto;
import com.laurapereyra.springcustomer.dto.ResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient("spring-account-client")
public interface AccountServiceClient {
    @GetMapping("/account-list/{customerId}")
    ResponseDto<?> customerClient(@PathVariable(name = "customerId") int customerId);
}
