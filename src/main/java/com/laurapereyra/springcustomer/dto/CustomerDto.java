package com.laurapereyra.springcustomer.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;


@Getter
@Setter
@NoArgsConstructor
@ToString
public class CustomerDto {
    private int customerId;
    private String name;
    private String lastname;
    private String cage;


    public CustomerDto(int customerId, String name, String lastname, String cage) {
        this.customerId = customerId;
        this.name = name;
        this.lastname = lastname;
        this.cage = cage;
    }
}
