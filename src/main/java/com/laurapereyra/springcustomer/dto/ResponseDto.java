package com.laurapereyra.springcustomer.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;


@Getter
@Setter
@NoArgsConstructor
@ToString
public class ResponseDto<T> implements Serializable {

    private static final long serialVersionUID = 1L;
    private String statusCode;
    private T response;
    private String errorDetail;


    public ResponseDto(String statusCode, T response, String errorDetail) {
        this.statusCode = statusCode;
        this.response = response;
        this.errorDetail = errorDetail;
    }
}
