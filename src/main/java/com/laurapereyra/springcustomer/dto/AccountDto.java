package com.laurapereyra.springcustomer.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class AccountDto {
    private int account_id;
    private String type_account;
    private String account_number;
    private BigDecimal amount_a;
    private BigDecimal amount_d;
    private BigDecimal pledged;
    private int customer_id;

    public AccountDto(int account_id, String type_account, String account_number, BigDecimal amount_a,
                      BigDecimal amount_d, BigDecimal pledged, int customer_id) {
        this.account_id = account_id;
        this.type_account = type_account;
        this.account_number = account_number;
        this.amount_a = amount_a;
        this.amount_d = amount_d;
        this.pledged = pledged;
        this.customer_id = customer_id;
    }
}
