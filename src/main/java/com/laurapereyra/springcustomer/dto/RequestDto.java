package com.laurapereyra.springcustomer.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;


@Getter
@Setter
@NoArgsConstructor
@ToString
public class RequestDto {
    private int requestId;
    private String method;
    private Date requestDate;
    private String username;
    private String host;
    private String response;
    private String request;
    private int status;
    private String txHost;
    private int txUserId;
    private Date txDate;


    public RequestDto(String method, String request, Date requestDate, String username, String host,
                      HttpServletRequest httpServletRequest) {
        this.method = method;
        this.request = request;
        this.requestDate = requestDate;
        this.username = username;
        this.host = host;
        this.status = 1;
        this.txUserId = 0; //request.getUserPrincipal().toString()
        this.txDate = new Date();
        this.txHost = httpServletRequest.getRemoteHost();
    }
}
