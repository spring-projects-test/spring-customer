package com.laurapereyra.springcustomer.mapper;

import com.laurapereyra.springcustomer.dto.RequestDto;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface RequestMapper {
    @Select("SELECT test:sq_request_id.nextval FROM test:tab")
    int getPkRequest();

    @Insert("INSERT INTO \n" +
            "  test:REQUEST (REQUEST_ID, METHOD, REQUEST, REQUEST_DATE, USERNAME, HOST,\n" +
            "  STATUS, TX_HOST, TX_USER_ID, TX_DATE)\n" +
            "  VALUES (#{requestId}, #{method}, #{request}, #{requestDate}, #{username}, #{host},\n " +
            "  #{status}, #{txHost}, #{txUserId}, #{txDate})")
    void persistRequest(RequestDto requestDto);
}
