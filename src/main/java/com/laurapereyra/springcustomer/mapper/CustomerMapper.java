package com.laurapereyra.springcustomer.mapper;

import com.laurapereyra.springcustomer.dto.CustomerDto;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface CustomerMapper {

    @Select("select * from customer \n" +
            "where customer_id = #{customerId}")
    CustomerDto getCustomerById(@Param("customerId") int customerId);

    @Insert({"INSERT INTO customer (\n" +
            "customer_id, name, lastname, cage\n" +
            ")\n" +
            "values (\n" +
            "#{customerId}, #{name}, #{lastname}, #{cage})\n"})
    void registerCustomer(CustomerDto customerDto);

    @Select("SELECT SQ_CUSTOMER_ID.nextval FROM tab")
    int getCustomerPk();
}
