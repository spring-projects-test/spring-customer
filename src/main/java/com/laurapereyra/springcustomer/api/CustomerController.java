package com.laurapereyra.springcustomer.api;

import com.laurapereyra.springcustomer.bl.CustomerBl;
import com.laurapereyra.springcustomer.dto.AccountDto;
import com.laurapereyra.springcustomer.dto.CustomerDto;
import com.laurapereyra.springcustomer.dto.RequestDto;
import com.laurapereyra.springcustomer.dto.ResponseDto;
import com.laurapereyra.springcustomer.service.AccountServiceClient;
import com.laurapereyra.springcustomer.util.ExceptionType;
import com.laurapereyra.springcustomer.util.HandlerException;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping
@DefaultProperties(defaultFallback = "fallback")
@Slf4j
public class CustomerController {

    CustomerBl customerBl;

    @Autowired()
    AccountServiceClient accountServiceClient;

    public CustomerController(CustomerBl customerBl) {
        this.customerBl = customerBl;
    }

    @HystrixCommand
    @GetMapping("/customer-account/{customerId}")
    public ResponseEntity<ResponseDto<AccountDto>> customerDetail(
            @PathVariable int customerId){
        List<AccountDto> result =  customerBl.getAccount(customerId);
        System.out.println("asdasdasdasd" + result);
        if(result !=null){
            ResponseDto<List<AccountDto>> response = new ResponseDto<>("MSSE-0000", result,null );
            return new ResponseEntity(response, HttpStatus.OK);
        }else{
            ResponseDto<List<AccountDto>> response = new ResponseDto<>("MSSE-0404", null,"Error" );
            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

    }

    @HystrixCommand
    @RequestMapping(path = "/customer/{customerId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto<?>>findCustomer(
            @PathVariable int customerId,
            @RequestParam("username") String username,
            @RequestParam("host") String host,
            @RequestParam("date") @DateTimeFormat(pattern="ddMMyyyyHHmmss") Date date,
            HttpServletRequest request){
        String requestLog = "CustomerId: "+customerId;
        String methodName = "findCustomer";
        RequestDto requestDto = new RequestDto(methodName, requestLog , date, username, host, request);
        ResponseDto<CustomerDto> result;
        CustomerDto customerDto = this.customerBl.getCustomerById(customerId,requestDto);
        if(customerDto != null){
            result = new ResponseDto<>("MSSE-0000", customerDto,null );
            return new ResponseEntity<>(result, HttpStatus.OK);
        }else{
            result = new ResponseDto<>("MSSE-0404",null,"Cliente no encontrado.");
            return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(path = "/customer-register", method = RequestMethod.POST)
    public ResponseEntity<ResponseDto<String>> customerRegister(@RequestBody CustomerDto customerDto){
        log.info("datos: "+customerDto);
        System.out.println("<<<<<"+customerDto+">>>>>");
        try {
            customerBl.customerRegister(customerDto);
            ResponseDto responseDto = new ResponseDto<>("MSSE-0000","Registrado correctamente",null);
            return new ResponseEntity<>(responseDto, HttpStatus.OK);

        } catch (Exception ex){
            log.warn("Error-Controller: Error al registrar cliente" , ex);
            ResponseDto responseDto = new ResponseDto<>("MSSE-500",null,"No se pudo registrar");
            return new ResponseEntity<>(responseDto, HttpStatus.NOT_ACCEPTABLE);
        }
    }

    ResponseEntity<ResponseDto<?>> fallback(Throwable throwable){
        //HandlerException handlerException = new HandlerException("error.......",throwable);
        ResponseDto result = new ResponseDto<>("MSSE-123",null,"qwertyuioqwertyui");
        return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
    }
}
